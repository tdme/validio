[![Npm version](https://badge.fury.io/js/validio.svg)](https://badge.fury.io/js/validio)
[![Magnum-CI](https://magnum-ci.com/status/26d62c99edfecaa3fcc5756af5a985b7.png)](https://magnum-ci.com/public/c204ad6dbe7168af5751/builds)

# [Validio](https://www.npmjs.com/package/validio)

> A NodeJS **type less schema validation**, and **model/schema** generator. Also, Validio can run as **Synchronous** and as **Promise** implementation too, see more at [Validio Npm](https://www.npmjs.com/package/validio).


## How to install
```bash
npm install --save validio
```

## How to use it
First we need to generate the schema to validate an object where `name` is a `string` which `max length` is `20`, and `field age` is a `int` which `max value` is `100`:

```javascript
var validio = require('validio')
validio.parse('req name 20, lastName 20, age int 100');
```

It will generate the next schema
```javascript
{
    name: {
        validators: {
            isString: {},
            isLength: {
                args: {length: 20}
            }
        },
        required: true,
        length: 20,
        type: 'string'
    },
    lastName: {
        validators: {
            isString: {},
            isLength: {
                args: {length: 20}
            }
        },
        length: 20,
        type: 'string'
    },
    age: {
        validators: {
            isInt: {
                args: {max: 100}
            }
        },
        type: 'int'
    }
}
```

In this case we will use the Person class, and will validate object instances using the schema:
```javascript
function Person(name, email, age, phone, birth, height) {
    this.name = name;
    this.email = email;
    this.age = age;
    this.phone = phone;
    this.birth = birth;
    this.height = height;
};
```

Then we can validate the object using the schema, in this case it will validate the `name` and `age` because those are the only fields that appears on the schema.
```javascript
var person = new Person('Joe', 'joe@joe.com', 45);

var result = validio.validate(schema, person);

if(!result.isValid) {
    console.log(result.errors.length == 1 ? 'There is an error:' : 'There are many errors:');
    for(error in result.errors)
        console.log(error);
}
else
    console.log('Validation succeeded');
```

## Validio as Promise
We can use Validio as Promise implementation, in the next sample we will use the same schema we build before.
```javascript
var validio = require('validio').aspromise;

var schema = validio.parse('name 20, age int 100');
var person = new Person('Joe', 'joe@joe.com', 45);


validio.validate(schema, person)
.then(function(result) {
    console.log('Validation succeeded');
})
.catch(function(result) {
    for(error of result.errors)
        console.log(error);
});
```

## New Features
 - Adding support for **ArrayTypes**
 - Fully supporting **Basic ArrayType**
 - Fully supporting **Schema Relations** with **ArrayType**

## Disclaimer

Strictly supporting packages up to next versions:
- [easy-process](https://www.npmjs.com/package/easy-process) v0.9.2
- [fs](https://www.npmjs.com/package/fs) v0.0.2
- [lodash](https://www.npmjs.com/package/lodash) v4.17.4
- [mkdirp](https://www.npmjs.com/package/mkdirp) v0.5.1
- [os](https://www.npmjs.com/package/os) v0.1.1
- [path](https://www.npmjs.com/package/path) v0.12.7
- [validator](https://www.npmjs.com/package/validator) v6.2.1
- [chai](https://www.npmjs.com/package/chai) v3.5.0
- [mocha](https://www.npmjs.com/package/mocha) v3.2.0
- [promisio](https://www.npmjs.com/package/promisio) v0.0.1
