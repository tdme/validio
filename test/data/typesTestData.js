var validio = require('../../index');

var // By default we are supporting leading ceros
    validInteger = [0, 1, 2, '13', '123', '0', '123', '-0', '+1', '01', '-01', '000', '0001'],
    invalidInteger = [undefined, null, '', '  ', 'a', 'a b c', '100e10', '123.123', {}, (new function(){})],
    validFloat = [
        '123',
        '123.',
        '123.123',
        '-123.123',
        '-0.123',
        '+0.123',
        '0.123',
        '.0',
        '01.123',
        '-0.22250738585072011e-307',
    ],
    invalidFloat = [
        undefined, null, '', '  ', 'a', 'a b c', '-.123', '.', 'foo', {}, (new function(){}),
    ],
    validDate = [
        // could accept year-month-day format
        '2017-01-20', '2017-01-01',
        ' 2017-01-20 ',     // Acepting spaces a side
        '2017-01-20T07:31:50.794Z',
        // will asume 01 as day
        '2017-12', '2017-11', '2017-10', '2017-09', '2017-8', '2017-7', '2017-1'
    ],
    invalidDate = [
        'aa', ' ', undefined, null,
        // Not numerics of any type
        '0', '1', '2', '3', '01', '02', '03', '004', '0005', '31',
        0, 1, 2 ,3, 01, 02, 03, 004, 0005, 31,
        148555291, 148555291788, 1485552917888,
        // could not accept year-month-day format
        '2017-20-01',
        // missing day
        '2017-13', '2017-0',
        // invalid month and day
        '2017-13-00', '2017-14-00', '2017-13-32', '2017-00-32',
        // invalid month
        '2017-13-01', '2017-14-01', '2017-00-01',
        // invalid day
        '2017-12-0', '2017-12-32', '2017-12-33', '2017-12-s',
        {},  (new function(){})
    ],
    validBoolean = ['true', 'false', '0', '1'],
    invalidBoolean = [
        undefined, null, '', '  ', 'a', 'a b c',
        '1.0', '0.0', 'true ', 'False', 'True', 'yes',
        {},  (new function(){})
    ],

    stampTime = new Date('2016-09-01'),
    //
    categoryInvalid = {name: 'Drama'},
    bookCategoryInvalid = {name: 'Book 2', author: 'James Bond', edition: '3', category: categoryInvalid, released: stampTime},
    bookCategoryInvalidEnforced = {name: 'Book 2', author: 'James Bond', edition: 3, category: categoryInvalid, released: stampTime},
    //
    categoryValid = {name: 'Novel', description:'All related to novel books'},
    categoryValid2 = {name: 'Science', description:'All related to science books'},
    bookCategoryValid = {name: 'Book 2', author: 'Jules Fountaint', edition: '3', category: categoryValid, released: stampTime},
    bookCategoryValidEnforced = {name: 'Book 2', author: 'Jules Fountaint', edition: 3, category: categoryValid, released: stampTime},
    //
    // This book has intentionaly book1.category field as string, book1.category field
    // is not related to category
    book1 = {name: 'Book 1', author: 'Jessie James', edition: '1', category:'novel', released: stampTime},
    book1Enforced = {name: 'Book 1', author: 'Jessie James', edition: 1, category:'novel', released: stampTime},
    //
    // This book is wrong because book.author is required and it is set to undefined
    bookWrong = {name: 'Book Wrong', author: undefined, edition: '2', category:'novel', released: new Date()},
    //
    // Sports to be raleted to students
    baseball = {name: 'Baseball', history: 'A great game over using a bat'},
    rugby = {name: 'Rugby', history: 'The father of American Football'},
    tennis = {name: 'Tennis', history: 'Extremely long time game for only two guys'},

    /**
     * Data types validations
     */
    dataTypes = [
        // Integers
        {
            title: 'Testing INT with basic valid values',
            schema: {field: 'myInt', arguments: 'int'},
            values: validInteger,
            mustPass: true
        },
        {
            title: 'Testing INT with basic valid values - ArrayType with no input array',
            schema: {field: 'myInt', arguments: '[int]'},
            values: validInteger,
            mustPass: false,
            message: 'myInt field is a sinlge value, but it is defined as array on schema'
        },
        {
            title: 'Testing INT with basic valid values - ArrayType with no definition',
            schema: {field: 'myInt', arguments: 'int'},
            values: [validInteger],
            mustPass: false,
            message: 'myInt field value is an array, but it is not defined as array'
        },
        {
            title: 'Testing INT with basic valid values - ArrayType with input array',
            schema: {field: 'myInt', arguments: '[int]'},
            values: [validInteger],
            mustPass: true
        },
        {
            title: 'Testing INT with invalid values',
            schema: {field: 'myInt', arguments: 'int'},
            values: invalidInteger,
            mustPass: false
        },
        {
            title: 'Testing INT with MAX value=10, every number over must fail',
            schema: {field: 'myInt', arguments: 'int 10'},
            values: [11, 12, 15, 100],
            mustPass: false
        },
        // Float
        {
            title: 'Testing FLOAT with basic valid values',
            schema: {field: 'myFloat', arguments: 'float'},
            values: validFloat,
            mustPass: true
        },
        {
            title: 'Testing FLOAT with basic valid values - ArrayType with no input array',
            schema: {field: 'myFloat', arguments: '[float]'},
            values: validFloat,
            mustPass: false,
            message: 'myFloat field is a sinlge value, but it is defined as array on schema'
        },
        {
            title: 'Testing FLOAT with basic valid values - ArrayType with no definition',
            schema: {field: 'myFloat', arguments: 'float'},
            values: [validFloat],
            mustPass: false,
            message: 'myFloat field value is an array, but it is not defined as array'
        },
        {
            title: 'Testing FLOAT with basic valid values - ArrayType with no definition',
            schema: {field: 'myFloat', arguments: '[float]'},
            values: [validFloat],
            mustPass: true
        },
        {
            title: 'Testing FLOAT with invalid values',
            schema: {field: 'myFloat', arguments: 'float'},
            values: invalidFloat,
            mustPass: false
        },
        {
            title: 'Testing FLOAT with MAX value=10.2, every number less or equal must pass',
            schema: {field: 'myFloat', arguments: 'float 10.2'},
            values: [-1, 0, 9, 10, 10.2],
            mustPass: true
        },
        {
            title: 'Testing FLOAT with MAX value=10.2, every number less or equal must pass - ArrayType with no input array',
            schema: {field: 'myFloat', arguments: '[float] 10.2'},
            values: [-1, 0, 9, 10, 10.2],
            mustPass: false
        },
        {
            title: 'Testing FLOAT with MAX value=10.2, every number over must fail',
            schema: {field: 'myFloat', arguments: 'float 10.2'},
            values: [10.3, 11, 12],
            mustPass: false
        },
        // Dates
        {
            title: 'Testing DATE with basic valid values',
            schema: {field: 'myDate', arguments: 'date'},
            values: validDate,
            mustPass: true
        },
        {
            title: 'Testing DATE with basic valid values - ArrayType with no input array',
            schema: {field: 'myDate', arguments: '[date]'},
            values: validDate,
            mustPass: false,
            message: 'myDate field is a sinlge value, but it is defined as array on schema'
        },
        {
            title: 'Testing DATE with basic valid values - ArrayType with no definition',
            schema: {field: 'myDate', arguments: 'date'},
            values: [validDate],
            mustPass: false,
            message: 'myDate field value is an array, but it is not defined as array'
        },
        {
            title: 'Testing DATE with basic valid values - ArrayType with input array',
            schema: {field: 'myDate', arguments: '[date]'},
            values: [validDate],
            mustPass: true
        },
        {
            title: 'Testing DATE with invalid values',
            schema: {field: 'myDate', arguments: 'date'},
            values: invalidDate,
            mustPass: false
        },

        // Booleans
        {
            title: 'Testing BOOLEAN with basic valid values',
            schema: {field: 'myBoolean', arguments: 'boolean'},
            values: validBoolean,
            mustPass: true
        },
        {
            title: 'Testing BOOLEAN with basic valid values - ArrayType with no input array',
            schema: {field: 'myBoolean', arguments: '[boolean]'},
            values: validBoolean,
            mustPass: false,
            message: 'myBoolean field is a sinlge value, but it is defined as array on schema'
        },
        {
            title: 'Testing BOOLEAN with basic valid values - ArrayType with no definition',
            schema: {field: 'myBoolean', arguments: 'boolean'},
            values: [validBoolean],
            mustPass: false,
            message: 'myBoolean field value is an array, but it is not defined as array'
        },
        {
            title: 'Testing BOOLEAN with basic valid values - ArrayType with input array',
            schema: {field: 'myBoolean', arguments: '[boolean]'},
            values: [validBoolean],
            mustPass: true
        },
        {
            title: 'Testing BOOLEAN with invalid values',
            schema: {field: 'myBoolean', arguments: 'boolean'},
            values: invalidBoolean,
            mustPass: false
        },

        // Related Objects
        {
            title: 'Testing OBJECT with valid object "Category"',
            schema: {field: 'myCategory', arguments: ' category'},
            dictionary: {category: validio.parse('r name, r description')},
            values: [categoryValid],
            mustPass: true
        },
        {
            title: 'Testing OBJECT with invalid object "Category"',
            schema: {field: 'myCategory', arguments: ' category'},
            dictionary: {
                category: validio.parse('r name, r description')
            },
            values: [categoryInvalid],
            mustPass: false
        },
        {
            title: 'Testing OBJECT with valid object "Category" - ArrayType with input array',
            schema: {field: 'myCategory', arguments: ' [category]'},
            dictionary: {
                category: validio.parse('r name, r description')
            },
            values: [[categoryValid]],
            mustPass: true
        },
        {
            title: 'Testing OBJECT with valid object "Category" - ArrayType with input array',
            schema: {field: 'myCategories', arguments: ' [category]'},
            dictionary: {
                category: validio.parse('r name, r description')
            },
            values: [[categoryValid, categoryValid2]],
            mustPass: true
        },
        {
            title: 'Testing OBJECT with "2 valid/1 invalid" object "Category" - ArrayType with input array',
            schema: {field: 'myCategories', arguments: ' [category]'},
            dictionary: {
                category: validio.parse('r name, r description')
            },
            values: [[categoryValid, categoryInvalid, categoryValid2]],
            mustPass: false
        },
        {
            title: 'Testing OBJECT with "2 valid/1 invalid in the last" object "Category" - ArrayType with many instances',
            schema: {field: 'myCategories', arguments: ' [category]'},
            dictionary: {
                category: validio.parse('r name, r description')
            },
            values: [[categoryValid, categoryValid2, categoryInvalid]],
            mustPass: false
        },
        {
            title: 'Testing OBJECT with "2 valid/1 invalid in the first" object "Category" - ArrayType with many instances',
            schema: {field: 'myCategories', arguments: ' [category]'},
            dictionary: {
                category: validio.parse('r name, r description')
            },
            values: [[categoryInvalid, categoryValid, categoryValid2]],
            mustPass: false
        }

    ],

    /**
     * Test for validating string values are converted to correct dataType as
     * expected on schema
     */
    dataTypesEnforcement = [
        {
            title: 'All non STRING values must be converted to string',
            schema: {field: 'myString', arguments: ''},
            values: ['hi ', 'how', ' are ', 'you', 101, {}]
        },
        // Integer
        {
            title: 'All INT valid values must be converted to Integer',
            schema: {field: 'myInt', arguments: 'int'},
            values: validInteger
        },
        // Float
        {
            title: 'All FLOAT valid values must be converted to Float',
            schema: {field: 'myFloat', arguments: 'float'},
            values: validFloat
        },
        // Dates
        {
            title: 'All DATE valid values must be converted to Date',
            schema: {field: 'myDate', arguments: 'date'},
            values: validDate
        },
        // Boolean
        {
            title: 'All BOOLEAN valid values must be converted to Boolean',
            schema: {field: 'myBoolean', arguments: 'boolean'},
            values: validBoolean
        },
    ],

    /**
     * This test will pass if Validio.validate validates correctly the test.input
     * object, and returned enforced object is deepEqual to test.expected object
     */
    schemaEnforcement = [
        {
            schema: {fields: 'r myString, r myInt int, r myFloat float, r myBoolean boolean'},
            input:    {myString: 'Hello', myInt: '10', myFloat: '5.5', myBoolean: 'true'},
            expected: {myString: 'Hello', myInt: 10, myFloat: 5.5, myBoolean: true}
        },
        {
            schema: {fields: 'r myString, r myInt int, r myFloat float, r myBoolean boolean, r myDate date'},
            input: {myString: 'Hello', myInt: '10', myFloat: '5.5', myBoolean: 'true',  myDate: '2017-03-15'},
            expected: {myString: 'Hello', myInt: 10, myFloat: 5.5, myBoolean: true,    myDate:new Date('2017-03-15')}
        },
        {
            schema: {fields: 'r myString, r myInt int, r myFloat float, r myBoolean boolean, r myDate date'},
            input: {myString: 'Hello', myInt: '06', myFloat: '6.0', myBoolean: 'true',  myDate: '2017-06'},
            expected: {myString: 'Hello', myInt: 6, myFloat: 6.0, myBoolean: true,    myDate:new Date('2017-06-01')}
        },
        {
            schema: {fields: 'r myString, r myInt int, r myFloat float, r myBoolean boolean, myDate date'},
            input: {myString: 'Bye bye', myInt: '20', myFloat: '6.5', myBoolean: 'false',  myDate: '2017-09'},
            expected: {myString: 'Bye bye', myInt: 20, myFloat: 6.5, myBoolean: false,    myDate:new Date('2017-09-01')}
        }
    ],

    /**
     * This test will pass if Validio.validate validates correctly the schemas relation
     * for inputTests.obj and returned enforced object is deepEqual to inputTests.expected
     * object
     */
    schemaRelation = [
        {
            schemas: [
                {name: 'book',      fields: 'r name, r author, r edition int, category, r released date'},
                {name: 'student',   fields: 'r name, r email f-email, r age int, isGeek boolean, height float, books book'}
            ],
            inputTests: [
                {
                    title: 'must pass when related book is not required and missed',
                    schema: 'student',
                    obj: {name: 'Jhon', email: 'jhon@jhon.com', age: '17'},
                    expectIsValid: true,
                    expected: {name: 'Jhon', email: 'jhon@jhon.com', age: 17}
                },
                {
                    title: 'must pass when realated book is undefined but not required',
                    schema: 'student',
                    obj: {name: 'Jhon', email: 'jhon@jhon.com', age: '18', isGeek:'true', height:'6.5', books:undefined},
                    expectIsValid: true,
                    expected: {name: 'Jhon', email: 'jhon@jhon.com', age: 18, isGeek:true, height:6.5}
                },
                {
                    title: 'must pass when related book is not required and instance is present',
                    schema: 'student',
                    obj: {name: 'Nick', email: 'nick@nick.com', age: '19', isGeek:'false', height:'7.5', books:book1},
                    expectIsValid: true,
                    obj: {name: 'Nick', email: 'nick@nick.com', age: 19, isGeek:false, height:7.5, books:book1}
                }
            ]
        },
        {
            schemas: [
                {name: 'book',      fields: 'r name, r author, r edition int, category, r released date'},
                {name: 'student',   fields: 'r name, r email f-email, r age int, isGeek boolean, height float, r books book'}
            ],
            inputTests: [
                {
                    title: 'must fail when related book is required and missed',
                    schema: 'student',
                    obj: {name: 'Jhon', email: 'jhon@jhon.com', age: '18'},
                    expectIsValid: false,
                },
                {
                    title: 'must pass when related book is required and instance is present',
                    schema: 'student',
                    obj: {name: 'Juan', email: 'juan@juan.com', age: '19', isGeek:'false', height:'8.5', books:book1},
                    expectIsValid: true,
                    expected: {name: 'Juan', email: 'juan@juan.com', age: 19, isGeek:false, height:8.5, books:book1Enforced}
                },
                {
                    title: 'must fail when related book is required and book has missing required author',
                    schema: 'student',
                    obj: {name: 'Nick', email: 'nick@nick.com', age: '20', isGeek:'true', height:'9.5', books:bookWrong},
                    expectIsValid: false
                }
            ]
        },
        {
            schemas: [
                {name: 'category',  fields: 'r name, r description'},
                {name: 'book',      fields: 'r name, r author, r edition int, r category category, r released date'},
                {name: 'student',   fields: 'r name, r email f-email, r age int, isGeek boolean, height float, r books book'}
            ],
            inputTests: [
                {
                    title: 'must fail when related book is using a invalid category',
                    schema: 'student',
                    obj: {name: 'Mary', email: 'mary@mary.com', age: '21', isGeek:'false', height:'10.5', books:book1},
                    expectIsValid: false,
                },
                {
                    title: 'must fail when related book is using category with missing required description',
                    schema: 'student',
                    obj: {name: 'Rosa', email: 'rosa@rosa.com', age: '23', isGeek:'false', height:'11.5', books:bookCategoryInvalid},
                    expectIsValid: false,
                },
                {
                    title: 'must pass when related book is using a correct category',
                    schema: 'student',
                    obj: {name: 'Victoria', email: 'victoria@victoria.com', age: '24', isGeek:'false', height:'12.5', books:bookCategoryValid},
                    expectIsValid: true,
                    expected: {name: 'Victoria', email: 'victoria@victoria.com', age: 24, isGeek:false, height:12.5, books:bookCategoryValidEnforced}
                },
            ]
        }
    ],

    // Test students with values on string a not in the correct dataType
    studentWithBook = {name:'Victoria', email:'victoria@victoria.com', age:'30', isGeek:'false', height:'13.5', books:bookCategoryValid, phone:'4095944659', mobile:'4083942639'},
    studentWithBookAndSport = {name:'Joshua', email:'joshua@joshua.com', age:'31', isGeek:'true', height:'14.5', books:bookCategoryValid, phone:'4105944610', mobile:'4113942611', sport:baseball},
    studentWithBookUptoSecondSport = {name:'Carlos', email:'carlos@carlos.com', age:'32', isGeek:'false', height:'15.5', books:bookCategoryValid, phone:'4125944612', mobile:'4133942613', sport:baseball, secondSport:tennis},
    // Expected students with no string values, insted it uses dataTypes as defined in the schema
    studentWithBookEnforced = {name:'Victoria', email:'victoria@victoria.com', age:30, isGeek:false, height:13.5, books:bookCategoryValidEnforced, phone:'4095944659', mobile:'4083942639'},
    studentWithBookAndSportEnforced = {name:'Joshua', email:'joshua@joshua.com', age:31, isGeek:true, height:14.5, books:bookCategoryValidEnforced, phone:'4105944610', mobile:'4113942611', sport:baseball},
    studentWithBookUptoSecondSportEnforced = {name:'Carlos', email:'carlos@carlos.com', age:32, isGeek:false, height:15.5, books:bookCategoryValidEnforced, phone:'4125944612', mobile:'4133942613', sport:baseball, secondSport:tennis},

    /**
     * This test will fail or pass depending if the fields are required or not,
     * we are going to loop over each field in the schema and dropping it. If the
     * field is required it must fail else case it will pass.
     * We are not going to test if failed on dataTypes, we are just simulating it
     * handles the relationship on many levels when each field is dropped.
     */
    schemaRelation3rdLevel = [
        {
            title: 'must pass when any required or not field is missing on Category level',
            dictionary: {
                category: validio.parse('name, description'),
                book:     validio.parse('name, author, edition int, category category, released date'),
                student:  validio.parse('name, email f-email, age int, isGeek boolean, height float, books book, phone f-phone, mobile f-mobilephone')
            },
            dropOn: 'books.category',
            loopOn: 'category',
            student: studentWithBook,
            studentExpected: studentWithBookEnforced,
        },
        {
            title: 'must pass when any required or not field is missing on Book level',
            dictionary: {
                category: validio.parse('name, description'),
                book:     validio.parse('name, author, edition int, category category, released date'),
                student:  validio.parse('name, email f-email, age int, isGeek boolean, height float, books book, phone f-phone, mobile f-mobilephone')
            },
            dropOn: 'books',
            loopOn: 'book',
            student: studentWithBook,
            studentExpected: studentWithBookEnforced,
        },
        {
            title: 'must pass when any required or not field is missing on Student level',
            dictionary: {
                category: validio.parse('name, description'),
                book:     validio.parse('name, author, edition int, category category, released date'),
                student:  validio.parse('name, email f-email, age int, isGeek boolean, height float, books book, phone f-phone, mobile f-mobilephone')
            },
            dropOn: '',
            loopOn: 'student',
            student: studentWithBook,
            studentExpected: studentWithBookEnforced,
        },
        {
            title: 'must pass when any required or not field is missing on Student level - related by book and sport',
            dictionary: {
                category: validio.parse('name, description'),
                book:     validio.parse('name, author, edition int, category category, released date'),
                sport:    validio.parse('name, history'),
                student:  validio.parse('name, email f-email, age int, isGeek boolean, height float, books book, phone f-phone, mobile f-mobilephone, sport sport')
            },
            dropOn: '',
            loopOn: 'student',
            student: studentWithBookAndSport,
            studentExpected: studentWithBookAndSportEnforced,
        },
        {
            title: 'must pass when any required or not field is missing on Student level - related by book, sport and secondSport',
            dictionary: {
                category: validio.parse('name, description'),
                book:     validio.parse('name, author, edition int, category category, released date'),
                sport:    validio.parse('name, history'),
                student:  validio.parse('name, email f-email, age int, isGeek boolean, height float, books book, phone f-phone, mobile f-mobilephone, sport sport, secondSport sport')
            },
            dropOn: '',
            loopOn: 'student',
            student: studentWithBookUptoSecondSport,
            studentExpected: studentWithBookUptoSecondSportEnforced,
        },
    ],


    /**
     * Tests for validating the extras argument "-e" is correctly parsed
     */
    extrasParsing = [
        // Integers
        {
            title: 'Testing INT must be correctly parsed when missing "-e" argument',
            schema: {field: 'myInt', arguments: 'int'},
            mustCreateExtras: false
        },
        {
            title: 'Testing INT must be correctly parsed when passing "-e" argument',
            schema: {field: 'myInt', arguments: 'int -e some extras'},
            mustCreateExtras: true
        },
        {
            title: 'Testing INT must be correctly parsed when passing "-e" argument and max length',
            schema: {field: 'myInt', arguments: 'int 10 -e some extras'},
            mustCreateExtras: true
        },
        {
            title: 'Testing INT must be correctly parsed when passing "-e" argument and ArrayType',
            schema: {field: 'myInt', arguments: '[int] -e some extras'},
            mustCreateExtras: true
        },
        // Integers
        {
            title: 'Testing FLOAT must be correctly parsed when missing "-e" argument',
            schema: {field: 'myFloat', arguments: 'float'},
            mustCreateExtras: false
        },
        {
            title: 'Testing FLOAT must be correctly parsed when passing "-e" argument',
            schema: {field: 'myFloat', arguments: 'float -e some extras'},
            mustCreateExtras: true
        },
        {
            title: 'Testing FLOAT must be correctly parsed when passing "-e" argument and max length',
            schema: {field: 'myFloat', arguments: 'float 10.2 -e some extras'},
            mustCreateExtras: true
        },
        {
            title: 'Testing FLOAT must be correctly parsed when passing "-e" argument and ArrayType',
            schema: {field: 'myFloat', arguments: '[float] -e some extras'},
            mustCreateExtras: true
        },
        // Dates
        {
            title: 'Testing DATE must be correctly parsed when missing "-e" argument',
            schema: {field: 'myDate', arguments: 'date'},
            mustCreateExtras: false
        },
        {
            title: 'Testing DATE must be correctly parsed when passing "-e" argument',
            schema: {field: 'myDate', arguments: 'date -e some extras'},
            mustCreateExtras: true
        },
        {
            title: 'Testing DATE must be correctly parsed when passing "-e" argument and max length',
            schema: {field: 'myDate', arguments: 'date -e some extras'},
            mustCreateExtras: true
        },
        {
            title: 'Testing DATE must be correctly parsed when passing "-e" argument and ArrayType',
            schema: {field: 'myDate', arguments: '[date] -e some extras'},
            mustCreateExtras: true
        },
        // Booleans
        {
            title: 'Testing BOOLEAN must be correctly parsed when missing "-e" argument',
            schema: {field: 'myBoolean', arguments: 'boolean'},
            mustCreateExtras: false
        },
        {
            title: 'Testing BOOLEAN must be correctly parsed when passing "-e" argument',
            schema: {field: 'myBoolean', arguments: 'boolean -e some extras'},
            mustCreateExtras: true
        },
        {
            title: 'Testing BOOLEAN must be correctly parsed when passing "-e" argument and max length',
            schema: {field: 'myBoolean', arguments: 'boolean -e some extras'},
            mustCreateExtras: true
        },
        {
            title: 'Testing BOOLEAN must be correctly parsed when passing "-e" argument and ArrayType',
            schema: {field: 'myBoolean', arguments: '[boolean] -e some extras'},
            mustCreateExtras: true
        },
        // Objects
        {
            title: 'Testing OBJECT must be correctly parsed when missing "-e" argument',
            schema: {field: 'myCategory', arguments: 'category'},
            mustCreateExtras: false
        },
        {
            title: 'Testing OBJECT must be correctly parsed when passing "-e" argument',
            schema: {field: 'myCategory', arguments: 'category -e some extras'},
            mustCreateExtras: true
        },
        {
            title: 'Testing OBJECT must be correctly parsed when passing "-e" argument and max length',
            schema: {field: 'myCategory', arguments: 'category -e some extras'},
            mustCreateExtras: true
        },
        {
            title: 'Testing OBJECT must be correctly parsed when passing "-e" argument and ArrayType',
            schema: {field: 'myCategory', arguments: '[category] -e some extras'},
            mustCreateExtras: true
        },
    ],

    /**
     * Tests for validating the extras argument "-e" is correctly parsed when passing
     * many fields.
     */
    extrasParsingOnManyFields = [
        // Many fields with -e option
        {
            title: 'Testing many fields with "-e" argument, myInt without -e',
            schema: [{field: 'myInt', arguments: 'myInt int', mustCreateExtras: false},
                     {field: 'myFloat', arguments: 'myFloat float -e some extra floats', mustCreateExtras: true},
                     {field: 'myDate', arguments: 'myDate date -e some extra floats', mustCreateExtras: true},
                    ]
        },
        {
            title: 'Testing many fields with "-e" argument, myInt with -e empty',
            schema: [{field: 'myInt', arguments: 'myInt int -e', mustCreateExtras: false},
                     {field: 'myFloat', arguments: 'myFloat float -e some extra floats', mustCreateExtras: true},
                     {field: 'myDate', arguments: 'myDate date -e some extra floats', mustCreateExtras: true},
                    ]
        },
        {
            title: 'Testing many fields with "-e" argument, myInt with -e white spaces',
            schema: [{field: 'myInt', arguments: 'myInt int -e "  "', mustCreateExtras: false},
                     {field: 'myFloat', arguments: 'myFloat float -e some extra floats', mustCreateExtras: true},
                     {field: 'myDate', arguments: 'myDate date -e some extra floats', mustCreateExtras: true},
                    ]
        },
        //
        {
            title: 'Testing many fields with "-e" argument, myFloat without -e',
            schema: [{field: 'myInt', arguments: 'myInt int -e some extra ints', mustCreateExtras: true},
                     {field: 'myFloat', arguments: 'myFloat float', mustCreateExtras: false},
                     {field: 'myDate', arguments: 'myDate date -e some extra floats', mustCreateExtras: true},
                    ]
        },
        {
            title: 'Testing many fields with "-e" argument, myFloat with -e empty',
            schema: [{field: 'myInt', arguments: 'myInt int -e some extra ints', mustCreateExtras: true},
                     {field: 'myFloat', arguments: 'myFloat float -e', mustCreateExtras: false},
                     {field: 'myDate', arguments: 'myDate date -e some extra floats', mustCreateExtras: true},
                    ]
        },
        {
            title: 'Testing many fields with "-e" argument, myFloat with -e white spaces',
            schema: [{field: 'myInt', arguments: 'myInt int -e some extra ints', mustCreateExtras: true},
                     {field: 'myFloat', arguments: 'myFloat float -e "  "', mustCreateExtras: false},
                     {field: 'myDate', arguments: 'myDate date -e some extra floats', mustCreateExtras: true},
                    ]
        },
        //
        {
            title: 'Testing many fields with "-e" argument, myDate without -e',
            schema: [{field: 'myInt', arguments: 'myInt int -e some extra ints', mustCreateExtras: true},
                     {field: 'myFloat', arguments: 'myFloat float -e some extra floats', mustCreateExtras: true},
                     {field: 'myDate', arguments: 'myDate date', mustCreateExtras: false},
                    ]
        },
        {
            title: 'Testing many fields with "-e" argument, myDate with -e empty',
            schema: [{field: 'myInt', arguments: 'myInt int -e some extra ints', mustCreateExtras: true},
                     {field: 'myFloat', arguments: 'myFloat float -e some extra floats', mustCreateExtras: true},
                     {field: 'myDate', arguments: 'myDate date -e', mustCreateExtras: false},
                    ]
        },
        {
            title: 'Testing many fields with "-e" argument, myDate with -e white spaces',
            schema: [{field: 'myInt', arguments: 'myInt int -e some extra ints', mustCreateExtras: true},
                     {field: 'myFloat', arguments: 'myFloat float -e some extra floats', mustCreateExtras: true},
                     {field: 'myDate', arguments: 'myDate date -e "  "', mustCreateExtras: false},
                    ]
        },
        // All with -e
        {
            title: 'Testing many fields with "-e" argument, all with -e',
            schema: [{field: 'myInt', arguments: 'myInt int -e some extra ints', mustCreateExtras: true},
                     {field: 'myFloat', arguments: 'myFloat float -e some extra floats', mustCreateExtras: true},
                     {field: 'myDate', arguments: 'myDate date -e some extra floats', mustCreateExtras: true},
                    ]
        },
        {
            title: 'Testing many fields with "-e" argument, myInt with max value',
            schema: [{field: 'myInt', arguments: 'myInt int 10 -e some extra ints', mustCreateExtras: true},
                     {field: 'myFloat', arguments: 'myFloat float -e some extra floats', mustCreateExtras: true},
                     {field: 'myDate', arguments: 'myDate date -e some extra floats', mustCreateExtras: true},
                    ]
        },
        {
            title: 'Testing many fields with "-e" argument, myFloat with max value',
            schema: [{field: 'myInt', arguments: 'myInt int -e some extra ints', mustCreateExtras: true},
                     {field: 'myFloat', arguments: 'myFloat float 10.2 -e some extra floats', mustCreateExtras: true},
                     {field: 'myDate', arguments: 'myDate date -e some extra floats', mustCreateExtras: true},
                    ]
        },

    ]
    ;


module.exports = {
    dataTypes: dataTypes,
    dataTypesEnforcement: dataTypesEnforcement,
    schemaEnforcement: schemaEnforcement,
    schemaRelation: schemaRelation,
    schemaRelation3rdLevel: schemaRelation3rdLevel,
    extrasParsing: extrasParsing,
    extrasParsingOnManyFields: extrasParsingOnManyFields
};
