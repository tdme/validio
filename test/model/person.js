//
// generated as: tdme add person -f "name 30, email f-email, age int, phone f-phone, birth date, height float"
//
function Person(name, email, age, phone, birth, height) {
    this.name = name;
    this.email = email;
    this.age = age;
    this.phone = phone;
    this.birth = birth;
    this.height = height;
};

module.exports = Person;
