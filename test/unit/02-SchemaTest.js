var validio = require('../../index'),
    Person = require('../model/person')
    ;

//name, email f-email, age int, phone f-phone, birth date, height float
//name 30, email f-email, age int, phone f-phone, birth date, height float
describe('As Synchronous', function() {
    describe('Schema validations given an object Person(name, email, age, phone, birth, height),', function() {
        describe('Data types,', function() {
            describe('String type on Person.name,', function() {
                var schemaString, schemaString10;

                it("must create correct schemas", function() {
                    schemaString = validio.parse('name');
                    expect(schemaString).to.deep.equal({
                        name: {
                            validators: {
                                isString: {}
                            },
                            type: 'string'
                        }
                    });
                    schemaString10 = validio.parse('name 10');
                    expect(schemaString10).to.deep.equal({
                        name: {
                            validators: {
                                isString: {},
                                isLength: {
                                    args: {length: 10}
                                }
                            },
                            length: 10,
                            type: 'string'
                        }
                    });
                });
                it("must validate a simple string", function() {
                    var person = new Person('Joe');

                    var result = validio.validate(schemaString, person);
                    expect(result).not.equal(undefined);
                    expect(result.isValid).to.be.true;
                    expect(result.errors.length).to.equal(0);
                });
                it("must validate a simple string length is not great than 10", function() {
                    var person = new Person('Joe');

                    var result = validio.validate(schemaString10, person);
                    expect(result).not.equal(undefined);
                    expect(result.isValid).to.be.true;
                    expect(result.errors.length).to.equal(0);

                    var person = new Person('Joe is more than 10 chars');
                    var result = validio.validate(schemaString10, person);
                    expect(result).not.equal(undefined);
                    expect(result.isValid).to.be.false;
                    expect(result.errors.length).to.equal(1);
                });

                it("must validate when passing an undefined object", function() {
                    var person = {email:''};

                    var result = validio.validate(schemaString, person);
                    expect(result).not.equal(undefined);
                    expect(result.isValid).to.be.true;
                    expect(result.errors.length == 0).to.be.true;
                });

                it("must validate when passing an undefined object to a 'length: 10' schema", function() {
                    var person = {email:''};

                    var result = validio.validate(schemaString10, person);
                    expect(result).not.equal(undefined);
                    expect(result.isValid).to.be.true;
                    expect(result.errors.length == 0).to.be.true;
                });
            });
            describe('Int type on Person.age,', function() {
                var schemaInt, schemaInt10;

                it("must create correct schemas", function() {
                    schemaInt = validio.parse('age int');
                    expect(schemaInt).to.deep.equal({
                        age: {
                            validators: {
                                isInt: {}
                            },
                            type: 'int'
                        }
                    });
                    schemaInt10 = validio.parse('age int 10');
                    expect(schemaInt10).to.deep.equal({
                        age: {
                            validators: {
                                isInt: {
                                    args: {max: 10}
                                }
                            },
                            type: 'int'
                        }
                    });
                });
                it("must validate a simple int", function() {
                    var person = new Person('Joe', '', 10);

                    var result = validio.validate(schemaInt, person);
                    expect(result).not.equal(undefined);
                    expect(result.isValid).to.be.true;
                    expect(result.errors.length).to.equal(0);
                });
                it("must validate a simple int length is less than 10", function() {
                    var person = new Person('Joe', '', 10);

                    var result = validio.validate(schemaInt10, person);
                    expect(result).not.equal(undefined);
                    expect(result.isValid).to.be.true;
                    expect(result.errors.length).to.equal(0);
                });
                it("must fail if a simple int length is great than 10", function() {
                    var person = new Person('Joe', '', 12);
                    var result = validio.validate(schemaInt10, person);
                    expect(result).not.equal(undefined);
                    expect(result.isValid).to.be.false;
                    expect(result.errors.length).to.equal(1);
                });
                it("must validate a simple int length is between 2 and 10", function() {

                    schemaInt10.age.validators.isInt.args = {min: 2, max: 10};

                    var person = new Person('Joe', '', 10);
                    var result = validio.validate(schemaInt10, person);
                    expect(result).not.equal(undefined);
                    expect(result.isValid).to.be.true;
                    expect(result.errors.length).to.equal(0);

                    var person = new Person('Joe', '', 1);
                    var result = validio.validate(schemaInt10, person);
                    expect(result).not.equal(undefined);
                    expect(result.isValid).to.be.false;
                    expect(result.errors.length).to.equal(1);

                    var person = new Person('Joe', '', 11);
                    var result = validio.validate(schemaInt10, person);
                    expect(result).not.equal(undefined);
                    expect(result.isValid).to.be.false;
                    expect(result.errors.length).to.equal(1);
                });
            });

            describe('Float type on Person.height,', function() {
                var schemaFloat, schemaFloat10;

                it("must create correct schemas", function() {
                    schemaFloat = validio.parse('height float');
                    expect(schemaFloat).to.deep.equal({
                        height: {
                            validators: {
                                isFloat: {}
                            },
                            type: 'float'
                        }
                    });
                    schemaFloat10 = validio.parse('height float 10.5');
                    expect(schemaFloat10).to.deep.equal({
                        height: {
                            validators: {
                                isFloat: {
                                    args: {max: 10.5}
                                }
                            },
                            type: 'float'
                        }
                    });
                });
                it("must validate a simple float", function() {
                    var person = new Person('Joe', '', 0, '', '', 10.5);

                    var result = validio.validate(schemaFloat, person);
                    expect(result).not.equal(undefined);
                    expect(result.isValid).to.be.true;
                    expect(result.errors.length).to.equal(0);
                });
                it("must validate a simple float length is less than 10.5", function() {
                    var person = new Person('Joe', '', 0, '', '', 10.5);

                    var result = validio.validate(schemaFloat10, person);
                    expect(result).not.equal(undefined);
                    expect(result.isValid).to.be.true;
                    expect(result.errors.length).to.equal(0);
                });
                it("must fail if a simple float length is great than 10.5", function() {
                    var person = new Person('Joe', '', 0, '', '', 12.3);
                    var result = validio.validate(schemaFloat10, person);
                    expect(result).not.equal(undefined);
                    expect(result.isValid).to.be.false;
                    expect(result.errors.length).to.equal(1);
                });
                it("must validate a simple float length is between 2.1 and 10.5", function() {

                    schemaFloat10.height.validators.isFloat.args = {min: 2.1, max: 10.5};

                    var person = new Person('Joe', '', 0, '', '', 10.5);
                    var result = validio.validate(schemaFloat10, person);
                    expect(result).not.equal(undefined);
                    expect(result.isValid).to.be.true;
                    expect(result.errors.length).to.equal(0);

                    var person = new Person('Joe', '', 0, '', '', 2.0);
                    var result = validio.validate(schemaFloat10, person);
                    expect(result).not.equal(undefined);
                    expect(result.isValid).to.be.false;
                    expect(result.errors.length).to.equal(1);

                    var person = new Person('Joe', '', 0, '', '', 11.5);
                    var result = validio.validate(schemaFloat10, person);
                    expect(result).not.equal(undefined);
                    expect(result.isValid).to.be.false;
                    expect(result.errors.length).to.equal(1);
                });
            });

        });

        describe('Formats,', function() {
            describe('Email format,', function() {
                var schemaEmail, schemaEmail10;

                it("must create correct schema", function() {
                    schemaEmail = validio.parse('email f-email');
                    expect(schemaEmail).to.deep.equal({
                        email: {
                            validators: {
                                isEmail: {},
                                isString: {}
                            },
                            format: 'email',
                            type: 'string'
                        }
                    });
                    schemaEmail10 = validio.parse('email 10 f-email');
                    expect(schemaEmail10).to.deep.equal({
                        email: {
                            validators: {
                                isEmail: {},
                                isString: {},
                                isLength: {
                                    args: {length: 10}
                                }
                            },
                            length: 10,
                            format: 'email',
                            type: 'string'
                        }
                    });
                });
                it("must validate an email format", function() {
                    var person = new Person('Joe', 'nick@nick.com');
                    var result = validio.validate(schemaEmail, person);

                    expect(result).not.equal(undefined);
                    expect(result.isValid).to.be.true;
                    expect(result.errors.length).to.equal(0);
                });
                it("must fail an invalid email format", function() {
                    var person = new Person('Joe', 'nick@nick.');
                    var result = validio.validate(schemaEmail, person);

                    expect(result).not.equal(undefined);
                    expect(result.isValid).to.be.false;
                    expect(result.errors.length).to.equal(1);
                });
                it("must fail an invalid email length great than 10", function() {
                    var person = new Person('Joe', 'nick@nick.com');
                    var result = validio.validate(schemaEmail10, person);

                    expect(result).not.equal(undefined);
                    expect(result.isValid).to.be.false;
                    expect(result.errors.length).to.equal(1);
                });
            });
        });

        describe('Many formats in the same command line', function() {
            var emailAndPhone, emai10lAndPhone, emai10lAndPhone8;
            it("must create correct schema", function() {
                emailAndPhone = validio.parse('email f-email, phone f-phone');
                expect(emailAndPhone).to.deep.equal(
                    {
                        email: {
                            validators: {
                                isEmail: {},
                                isString: {}
                            },
                            format: 'email',
                            type: 'string'
                        },
                        phone: {
                            validators: {
                                isMobilePhone: {args: ['en-US']},
                                isString: {}
                            },
                            format: 'phone',
                            type: 'string'
                        }
                    }
                );
                emai10lAndPhone8 = validio.parse('email 10 f-email, phone 8 f-phone');
                expect(emai10lAndPhone8).to.deep.equal(
                    {
                        email: {
                            validators: {
                                isEmail: {},
                                isString: {},
                                isLength: {
                                    args: {length: 10}
                                }
                            },
                            length: 10,
                            format: 'email',
                            type: 'string'
                        },
                        phone: {
                            validators: {
                                isMobilePhone: {args: ['en-US']},
                                isString: {},
                                isLength: {
                                    args: {length: 8}
                                }
                            },
                            length: 8,
                            format: 'phone',
                            type: 'string'
                        }
                    }
                );
            });
            it("must validate an email format and phone format", function() {
                var person = new Person('Joe', 'nick@nick.com', 10, '4104101000');
                var result = validio.validate(emailAndPhone, person);

                expect(result).not.equal(undefined);
                expect(result.isValid).to.be.true;
                expect(result.errors.length).to.equal(0);
            });
            it("must fail an invalid email and phone format", function() {
                // var person = new Person('Joe', 'nick@nick.');
                var person = new Person('Joe', 'nick@nick.', 10, '410 4101000');
                var result = validio.validate(emailAndPhone, person);

                expect(result).not.equal(undefined);
                expect(result.isValid).to.be.false;
                expect(result.errors.length).to.equal(2);
            });
            it("must fail an invalid email length great than 10 and invalid phone great than 8", function() {
                var person = new Person('Joe', 'nick@nick.com', 10, '4104101000');
                var result = validio.validate(emai10lAndPhone8, person);

                expect(result).not.equal(undefined);
                expect(result.isValid).to.be.false;
                expect(result.errors.length).to.equal(2);
            });
        });

        describe('Many required fields in the same command line', function() {
            var schemaReqEmailAndPhone, schemaReqEmailAndReqPhone;

            it("must create correct schema", function() {
                schemaReqEmailAndPhone = validio.parse('req email f-email, phone f-phone');
                expect(schemaReqEmailAndPhone).to.deep.equal({
                    email: {
                        validators: {
                            isEmail: {},
                            isString: {}
                        },
                        format: 'email',
                        type: 'string',
                        required: true
                    },
                    phone: {
                        validators: {
                            isMobilePhone: {args: ['en-US']},
                            isString: {}
                        },
                        format: 'phone',
                        type: 'string'
                    }
                });
                schemaReqEmailAndReqPhone = validio.parse('req email f-email, req phone f-phone');
                expect(schemaReqEmailAndReqPhone).to.deep.equal({
                    email: {
                        validators: {
                            isEmail: {},
                            isString: {}
                        },
                        format: 'email',
                        type: 'string',
                        required: true
                    },
                    phone: {
                        validators: {
                            isMobilePhone: {args: ['en-US']},
                            isString: {}
                        },
                        format: 'phone',
                        type: 'string',
                        required: true
                    }
                });
            });
            it("must validate a required email format and phone format", function() {
                var person = new Person('Joe', 'nick@nick.com', 10, '4104101000');
                var result = validio.validate(schemaReqEmailAndPhone, person);

                expect(result).not.equal(undefined);
                expect(result.isValid).to.be.true;
                expect(result.errors.length).to.equal(0);
            });
            it("must fail with missing required email format and phone format", function() {
                var person = new Person('Joe', '', 10, '4104101000');
                var result = validio.validate(schemaReqEmailAndPhone, person);

                expect(result).not.equal(undefined);
                expect(result.isValid).to.be.false;
                expect(result.errors.length).to.equal(1);
            });

            xit("must fail when passing an undefined object", function() {
                var person = {email:''};

                var result = validio.validate(schemaString, person);
                expect(result).not.equal(undefined);
                expect(result.isValid).to.be.false;
                expect(result.errors.length > 0).to.be.true;
            });

            xit("must fail when passing an undefined object to a 'length: 10' schema", function() {
                var person = {email:''};

                var result = validio.validate(schemaString10, person);
                expect(result).not.equal(undefined);
                expect(result.isValid).to.be.false;
                expect(result.errors.length > 0).to.be.true;
            });

        });

    });
});

// Person = name, email, age, phone, birth, height
