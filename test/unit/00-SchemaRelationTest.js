var validio = require('../../index'),
    testData = require('../data/typesTestData'),
    required = 'r',
    enforce = true,
    promisio = require('promisio'),
    path = require('path'),
    _ = require('lodash')
    ;

//name, email f-email, age int, phone f-phone, birth date, height float
//name 30, email f-email, age int, phone f-phone, birth date, height float
describe('Schema relationships', ()=> {
    var schemaRelationTest = testData.schemaRelation

    schemaRelationTest.forEach((test)=> {

        describe('Validating level relationships', ()=> {
            var schemas = test.schemas,
                inputs = test.inputTests,
                dictionary = {};

            schemas.forEach((schema)=> {
                it('must generate "' +schema.name+ '" schema', ()=> {
                    // Build the schema
                    dictionary[schema.name] = validio.parse(schema.fields);
                });
            });

            inputs.forEach((input)=> {
                it(input.title, ()=> {
                    var result = validio.validate(dictionary[input.schema], input.obj, {enforceTypes: true, dictionary: dictionary});

                    expect(result).not.equal(undefined);
                    expect(result.isValid).equal(input.expectIsValid);

                    if(result.isValid && input.expected)
                        expect(result.enforcedObj).deep.equal(input.expected);
                });
            });
        });
    });
});

describe('Schema relationships - many levels test', ()=> {

    var schemaRelation3rdTest = testData.schemaRelation3rdLevel//.slice(0, 3);

    schemaRelation3rdTest.forEach((test)=> {

        describe(test.title, ()=> {
            var dictionary = test.dictionary,
                dictionaryOn = dictionary[test.loopOn],
                student,
                studentExpected,
                result;

            (_.keys(dictionaryOn)).forEach((fieldName)=> {
                var fieldSchema = dictionaryOn[fieldName],
                    must = fieldSchema.required ? "must fail" : "must pass",
                    required = fieldSchema.required ? "required" : "not required"
                    ;

                it('must fail when dropping field "' +fieldName+ '" and it is required', ()=> {
                    student = _.cloneDeep(test.student);

                    /**
                     * Get the object give a xpath like: 'student.books.category' or 'student.books'
                     */
                    var target = xpathObject(student, test.dropOn)
                    delete target[fieldName];

                    // Force the field schema to be required
                    fieldSchema.required = true

                    result = validio.validate(dictionary.student, student, {enforceTypes: true, dictionary: dictionary});
                    expect(result).not.equal(undefined);
                    expect(result.isValid).equal(false);
                });

                it('must enforce conversion as expected in the schema', ()=> {
                    if(result.isValid) {
                        studentExpected = _.cloneDeep(test.studentExpected);
                        var target = xpathObject(studentExpected, test.dropOn)
                        delete target[fieldName];

                        expect(result.enforcedObj).deep.equal(studentExpected);
                    }
                });

                it('must pass when dropping field "' +fieldName+ '" and it is not required', ()=> {
                    student = _.cloneDeep(test.student);

                    /**
                     * Get the object give a xpath like: 'student.books.category' or 'student.books'
                     */
                    var target = xpathObject(student, test.dropOn)
                    delete target[fieldName];

                    // Force the field schema to be not required
                    fieldSchema.required = false

                    result = validio.validate(dictionary.student, student, {enforceTypes: true, dictionary: dictionary});
                    expect(result).not.equal(undefined);
                    expect(result.isValid).equal(true);
                });

                it('must enforce conversion as expected in the schema', ()=> {
                    if(result.isValid) {
                        studentExpected = _.cloneDeep(test.studentExpected);
                        var target = xpathObject(studentExpected, test.dropOn)
                        delete target[fieldName];

                        expect(result.enforcedObj).deep.equal(studentExpected);
                    }
                });
            });
        });
    });
});

function xpathObject(obj, xpath) {
    return (xpath && xpath != '') ? _.map([obj], _.property(xpath))[0] : obj;
}
