var validio = require('../../index'),
    testData = require('../data/typesTestData'),
    required = 'r', enforce = true
    ;

//name, email f-email, age int, phone f-phone, birth date, height float
//name 30, email f-email, age int, phone f-phone, birth date, height float
describe('Data type enforcement', ()=> {
    var enforcementTest = testData.dataTypesEnforcement

    enforcementTest.forEach((test)=> {

        describe(test.title, ()=> {
            // Build the schema with always required
            var schema = validio.parse(required + ' ' +test.schema.field+ ' ' +test.schema.arguments),
                result;

            test.values.forEach((value)=> {
                it('must enforce conversion of "' +value+ '" to ' +schema[test.schema.field].type, ()=> {
                    var obj = {}
                    obj[test.schema.field] = value
                    result = validio.validate(schema, obj, {enforceTypes: true});

                    expect(result).not.equal(undefined);
                    expect(result.isValid).equal(true);
                });
            });
        });
    });

    var enforcementTest = testData.schemaEnforcement

    describe('Enforcing schema dataType conversion', ()=> {

        enforcementTest.forEach((test)=> {
            // Build the schema with always required
            var schema = validio.parse(test.schema.fields),
                result;

            it('must convert "' +test.schema.fields+ '" ', ()=> {
                result = validio.validate(schema, test.input, {enforceTypes: true});
                
                expect(result).not.equal(undefined);
                expect(result.isValid).equal(true);
                expect(result.enforcedObj).deep.equal(test.expected);
            });
        });
    });
});
