var validio = require('../../index');

describe('As Synchronous', function() {
    describe('Parser using types', function() {
        describe('String type', function() {
            it("must return 'type=string,validators=isString' when passing 'name'", function() {
                var field = validio.parse('name');
                expect(field.name).to.deep.equal({
                    validators: {
                        isString: {}
                    },
                    type: 'string'
                });
            })
            it("must return 'type=string,length=10,validators=isLength,isString' when passing 'name 10'", function() {
                var field = validio.parse('name 10');
                expect(field.name).to.deep.equal({
                    validators: {
                        isLength: {
                            args: {length: 10}
                        },
                        isString: {}
                    },
                    length: 10,
                    type: 'string'
                });
            })
        });

        describe('Date type', function() {
            it("must return 'type=date,validators=isDate' when passing 'myDate date'", function() {
                var field = validio.parse('myDate date');
                expect(field.myDate).to.deep.equal({
                    validators: {
                        isDate: {}
                    },
                    type: 'date'
                });
            })
        });

        describe('Int type', function() {
            it("must return 'type=int,validators=isInt' when passing 'myInt int'", function() {
                var field = validio.parse('myInt int');
                expect(field.myInt).to.deep.equal({
                    validators: {
                        isInt: {}
                    },
                    type: 'int'
                });
            });
            it("must return 'type=int,validators=isInt' when passing 'myInt int 10'", function() {
                var field = validio.parse('myInt int 10');
                expect(field.myInt).to.deep.equal({
                    validators: {
                        isInt: {
                            args: {max: 10}
                        }
                    },
                    type: 'int'
                });
            });
            it("must return 'type=int,validators=isInt' when passing invalid input 'myInt xx yy int 10'", function() {
                schemaInt10 = validio.parse('myInt xx yy int 10');
                expect(schemaInt10).to.deep.equal({
                    myInt: {
                        validators: {
                            isInt: {
                                args: {max: 10}
                            }
                        },
                        type: 'int'
                    }
                });
            });
        });

        describe('Boolean type', function() {
            it("must return 'type=boolea,validators=isBoolean' when passing 'myBoolean boolean'", function() {
                var field = validio.parse('myBoolean boolean');
                expect(field.myBoolean).to.deep.equal({
                    validators: {
                        isBoolean: {}
                    },
                    type: 'boolean'
                });
            })
        });

        describe('Decimal type', function() {
            it("must return 'type=decimal,validators=isDecimal' when passing 'myDecimale decimal'", function() {
                var field = validio.parse('myDecimale decimal');
                expect(field.myDecimale).to.deep.equal({
                    validators: {
                        isDecimal: {}
                    },
                    type: 'decimal'
                });
            })
        });

        describe('Float type', function() {
            it("must return 'type=float,validators=isFloat' when passing 'myFloat float'", function() {
                var field = validio.parse('myFloat float');
                expect(field.myFloat).to.deep.equal({
                    validators: {
                        isFloat: {}
                    },
                    type: 'float'
                });
            })
        });

        describe('Numeric type', function() {
            it("must return 'type=numeric,validators=isNumeric' when passing 'myNumeric numeric'", function() {
                var field = validio.parse('myNumeric numeric');
                expect(field.myNumeric).to.deep.equal({
                    validators: {
                        isNumeric: {}
                    },
                    type: 'numeric'
                });
            })
        });
    });

    describe('Parser using formats', function() {
        describe('Email format', function() {
            it("must return 'type=string,validators=isEmail' when passing 'myEmail f-email'", function() {
                var field = validio.parse('myEmail f-email');
                expect(field.myEmail).to.deep.equal({
                    validators: {
                        isEmail: {},
                        isString: {}
                    },
                    format: 'email',
                    type: 'string'
                });
            });
            it("must return 'type=string,length=10,validators=isEmail' when passing 'myEmail 10 f-email'", function() {
                var field = validio.parse('myEmail 10 f-email');
                expect(field.myEmail).to.deep.equal({
                    validators: {
                        isEmail: {},
                        isString: {},
                        isLength: {
                            args: {length: 10}
                        }
                    },
                    length: 10,
                    format: 'email',
                    type: 'string'
                });
            });
            it("must return 'type=string,length=10,validators=isEmail' when passing 'myEmail xx yy 10 f-email'", function() {
                var field = validio.parse('myEmail xx yy 10 f-email');
                expect(field.myEmail).to.deep.equal({
                    validators: {
                        isEmail: {},
                        isString: {},
                        isLength: {
                            args: {length: 10}
                        }
                    },
                    length: 10,
                    format: 'email',
                    type: 'string'
                });
            });
        });

        describe('Phone format', function() {
            it("must return 'type=string,validators=isMobilePhone' when passing 'myPhone f-phone'", function() {
                var field = validio.parse('myPhone f-phone');
                expect(field.myPhone).to.deep.equal({
                    validators: {
                        isMobilePhone: {args: ['en-US']},
                        isString: {}
                    },
                    format: 'phone',
                    type: 'string'
                });
            })
            it("must return 'type=string,length=10,validators=isMobilePhone' when passing 'myPhone 10 f-phone'", function() {
                var field = validio.parse('myPhone 10 f-phone');
                expect(field.myPhone).to.deep.equal({
                    validators: {
                        isMobilePhone: {args: ['en-US']},
                        isString: {},
                        isLength: {
                            args: {length: 10}
                        }
                    },
                    length: 10,
                    format: 'phone',
                    type: 'string'
                });
            })
        });

        describe('Make formats fail: ', function() {
            it("must asume string type 'type=string,validators=isString', when invalid format 'myEmail f-Xemail'", function() {
                var field = validio.parse('myEmail f-Xemail');
                expect(field.myEmail).to.deep.equal({
                    validators: {
                        isString: {}
                    },
                    type: 'string'
                });
            });
            it("must asume string type 'type=string,length=10,validators=isString', when invalid format 'myEmail 10 f-Xemail'", function() {
                var field = validio.parse('myEmail 10 f-Xemail');
                expect(field.myEmail).to.deep.equal({
                    validators: {
                        isString: {},
                        isLength: {
                            args: {length: 10}
                        }
                    },
                    length: 10,
                    type: 'string'
                });
            });
        })

        describe('Many formats in the same command line', function() {
            it("must return 'myEmail and myPhone' when passing 'myEmail f-email,myPhone f-phone'", function() {
                var fields = validio.parse('myEmail f-email, myPhone f-phone');
                expect(fields).to.deep.equal(
                    {
                        myEmail: {
                            validators: {
                                isEmail: {},
                                isString: {}
                            },
                            format: 'email',
                            type: 'string'
                        },
                        myPhone: {
                            validators: {
                                isMobilePhone: {args: ['en-US']},
                                isString: {}
                            },
                            format: 'phone',
                            type: 'string'
                        }
                    }
                );
            });
            it("must return 'myEmail 10 and myPhone' when passing 'myEmail 10 f-email,myPhone f-phone'", function() {
                var fields = validio.parse('myEmail 10 f-email, myPhone f-phone');
                expect(fields).to.deep.equal(
                    {
                        myEmail: {
                            validators: {
                                isEmail: {},
                                isString: {},
                                isLength: {
                                    args: {length: 10}
                                }
                            },
                            length: 10,
                            format: 'email',
                            type: 'string'
                        },
                        myPhone: {
                            validators: {
                                isMobilePhone: {args: ['en-US']},
                                isString: {}
                            },
                            format: 'phone',
                            type: 'string'
                        }
                    }
                );
            });
            it("must return 'myEmail 10 and myPhone 15' when passing 'myEmail 10 f-email,myPhone 15 f-phone'", function() {
                var fields = validio.parse('myEmail 10 f-email, myPhone 15 f-phone');
                expect(fields).to.deep.equal(
                    {
                        myEmail: {
                            validators: {
                                isEmail: {},
                                isString: {},
                                isLength: {
                                    args: {length: 10}
                                }
                            },
                            length: 10,
                            format: 'email',
                            type: 'string'
                        },
                        myPhone: {
                            validators: {
                                isMobilePhone: {args: ['en-US']},
                                isString: {},
                                isLength: {
                                    args: {length: 15}
                                }
                            },
                            length: 15,
                            format: 'phone',
                            type: 'string'
                        }
                    }
                );
            });

        });
    });

    describe("Pasrser using required fields", function() {
        describe('String type', function() {
            it("must return 'type=string,validators=isString,required=true' when passing 'req name'", function() {
                var field = validio.parse('req name');
                expect(field.name).to.deep.equal({
                    validators: {
                        isString: {}
                    },
                    type: 'string',
                    required: true
                });
            })
            it("must return 'type=string,length=10,validators=isLength,isString, required=true' when passing 'r name 10'", function() {
                var field = validio.parse('r name 10');
                expect(field.name).to.deep.equal({
                    validators: {
                        isLength: {
                            args: {length: 10}
                        },
                        isString: {}
                    },
                    length: 10,
                    type: 'string',
                    required: true
                });
            })
        });

        describe('Date type', function() {
            it("must return 'type=date,validators=isDate, required=true' when passing 'req myDate date'", function() {
                var field = validio.parse('req myDate date');
                expect(field.myDate).to.deep.equal({
                    validators: {
                        isDate: {}
                    },
                    type: 'date',
                    required: true
                });
            })
        });

        describe('Int type', function() {
            it("must return 'type=int,validators=isInt, required=true' when passing 'req myInt int'", function() {
                var field = validio.parse('req myInt int');
                expect(field.myInt).to.deep.equal({
                    validators: {
                        isInt: {}
                    },
                    type: 'int',
                    required: true
                });
            });
            it("must return 'type=int,validators=isInt, required=true' when passing 'r myInt int 10'", function() {
                var field = validio.parse('r myInt int 10');
                expect(field.myInt).to.deep.equal({
                    validators: {
                        isInt: {
                            args: {max: 10}
                        }
                    },
                    type: 'int',
                    required: true
                });
            });
            it("must return 'type=int,validators=isInt, required=true' when passing invalid input 'myInt xx yy int 10'", function() {
                schemaInt10 = validio.parse('r myInt xx yy int 10');
                expect(schemaInt10).to.deep.equal({
                    myInt: {
                        validators: {
                            isInt: {
                                args: {max: 10}
                            }
                        },
                        type: 'int',
                        required: true
                    }
                });
            });
        });

        describe('Boolean type', function() {
            it("must return 'type=boolea,validators=isBoolean, required=true' when passing 'req myBoolean boolean'", function() {
                var field = validio.parse('req myBoolean boolean');
                expect(field.myBoolean).to.deep.equal({
                    validators: {
                        isBoolean: {}
                    },
                    type: 'boolean',
                    required: true
                });
            })
        });

        describe('Decimal type', function() {
            it("must return 'type=decimal,validators=isDecimal, required=true' when passing 'req myDecimale decimal'", function() {
                var field = validio.parse('req myDecimale decimal');
                expect(field.myDecimale).to.deep.equal({
                    validators: {
                        isDecimal: {}
                    },
                    type: 'decimal',
                    required: true
                });
            })
        });

        describe('Float type', function() {
            it("must return 'type=float,validators=isFloat, required=true' when passing 'req myFloat float'", function() {
                var field = validio.parse('req myFloat float');
                expect(field.myFloat).to.deep.equal({
                    validators: {
                        isFloat: {}
                    },
                    type: 'float',
                    required: true
                });
            })
        });

        describe('Numeric type', function() {
            it("must return 'type=numeric,validators=isNumeric, required=true' when passing 'req myNumeric numeric'", function() {
                var field = validio.parse('req myNumeric numeric');
                expect(field.myNumeric).to.deep.equal({
                    validators: {
                        isNumeric: {}
                    },
                    type: 'numeric',
                    required: true
                });
            })
        });

        describe("Many required fields in the same command line", function() {
            it("must return 'email required and phone' when passing 'req email f-email,phone f-phone'", function() {
                var fields = validio.parse('req email f-email, phone f-phone');
                expect(fields).to.deep.equal(
                    {
                        email: {
                            validators: {
                                isEmail: {},
                                isString: {}
                            },
                            format: 'email',
                            type: 'string',
                            required: true
                        },
                        phone: {
                            validators: {
                                isMobilePhone: {args: ['en-US']},
                                isString: {}
                            },
                            format: 'phone',
                            type: 'string'
                        }
                    }
                );
            });
            it("must return 'email required and phone required' when passing 'req email f-email, req phone f-phone'", function() {
                var fields = validio.parse('req email f-email, req phone f-phone');
                expect(fields).to.deep.equal(
                    {
                        email: {
                            validators: {
                                isEmail: {},
                                isString: {}
                            },
                            format: 'email',
                            type: 'string',
                            required: true
                        },
                        phone: {
                            validators: {
                                isMobilePhone: {args: ['en-US']},
                                isString: {}
                            },
                            format: 'phone',
                            type: 'string',
                            required: true
                        }
                    }
                );
            });
        });
    })
});
