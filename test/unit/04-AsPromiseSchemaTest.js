var validio = require('../../index').aspromise,
    Person = require('../model/person')
    ;

function dummySucced(result) {
}
function cacthAndDoneTheError(result) {
    done(new Error(result.errors));
}

//name, email f-email, age int, phone f-phone, birth date, height float
//name 30, email f-email, age int, phone f-phone, birth date, height float
describe('As Promise', function() {
    describe('Schema validations given an object Person(name, email, age, phone, birth, height),', function() {
        describe('Data types,', function() {
            describe('String type on Person.name,', function() {
                var schemaString, schemaString10, schemaEmail;

                it("must create correct schemas", function() {
                    schemaString = validio.parse('name');
                    expect(schemaString).to.deep.equal({
                        name: {
                            validators: {
                                isString: {}
                            },
                            type: 'string'
                        }
                    });
                    schemaString10 = validio.parse('name 10');
                    expect(schemaString10).to.deep.equal({
                        name: {
                            validators: {
                                isString: {},
                                isLength: {
                                    args: {length: 10}
                                }
                            },
                            length: 10,
                            type: 'string'
                        }
                    });
                });
                it("must validate a simple string", function(done) {
                    var person = new Person('Joe');

                    validio.validate(schemaString, person)
                    .then(function(result) {
                        expect(result).not.equal(undefined);
                        expect(result.isValid).to.be.true;
                        expect(result.errors.length).to.equal(0);
                    })
                    .then(done)
                    .catch(cacthAndDoneTheError);
                });
                it("must validate a simple string length is not great than 10", function(done) {
                    var person = new Person('Joe');

                    validio.validate(schemaString10, person)
                    .then(function(result) {
                        expect(result).not.equal(undefined);
                        expect(result.isValid).to.be.true;
                        expect(result.errors.length).to.equal(0);
                    })
                    .then(done)
                    .catch(cacthAndDoneTheError);
                });
            });
            describe('Int type on Person.age,', function() {
                var schemaInt, schemaInt10;

                it("must create correct schemas", function() {
                    schemaInt = validio.parse('age int');
                    expect(schemaInt).to.deep.equal({
                        age: {
                            validators: {
                                isInt: {}
                            },
                            type: 'int'
                        }
                    });
                    schemaInt10 = validio.parse('age int 10');
                    expect(schemaInt10).to.deep.equal({
                        age: {
                            validators: {
                                isInt: {
                                    args: {max: 10}
                                }
                            },
                            type: 'int'
                        }
                    });
                });
                it("must validate a simple int", function(done) {
                    var person = new Person('Joe', '', 10);

                    validio.validate(schemaInt, person)
                    .then(function(result) {
                        expect(result).not.equal(undefined);
                        expect(result.isValid).to.be.true;
                        expect(result.errors.length).to.equal(0);
                    })
                    .then(done)
                    .catch(cacthAndDoneTheError);
                });
                it("must validate a simple int length is less than 10", function(done) {
                    var person = new Person('Joe', '', 10);

                    validio.validate(schemaInt10, person)
                    .then(function(result) {
                        expect(result).not.equal(undefined);
                        expect(result.isValid).to.be.true;
                        expect(result.errors.length).to.equal(0);
                    })
                    .then(done)
                    .catch(cacthAndDoneTheError);
                });
                it("must fail if a simple int length is great than 10", function(done) {
                    var person = new Person('Joe', '', 12);

                    validio.validate(schemaInt10, person)
                    .catch(function(result) {
                        expect(result).not.equal(undefined);
                        expect(result.isValid).to.be.false;
                        expect(result.errors.length).to.equal(1);
                        done();
                    });
                });
                it("must validate a simple int length is between 2 and 10", function(done) {

                    schemaInt10.age.validators.isInt.args = {min: 2, max: 10};

                    var person = new Person('Joe', '', 10);
                    validio.validate(schemaInt10, person)
                    .then(succedBecauseAgeIsWithin2and10)
                    .then(runWithAgeEqual1)
                    .then(dummySucced, failBecauseAgeIs1andLessThan2)
                    .then(runWithAgeEqual11)
                    .then(dummySucced, failBecauseAgeIs11andGreatThan10)
                    .then(done)
                    .catch(cacthAndDoneTheError);

                    function succedBecauseAgeIsWithin2and10(result) {
                        expect(result).not.equal(undefined);
                        expect(result.isValid).to.be.true;
                        expect(result.errors.length).to.equal(0);
                    }
                    function runWithAgeEqual1(result) {
                        var person = new Person('Joe', '', 1);
                        return validio.validate(schemaInt10, person);
                    }
                    function failBecauseAgeIs1andLessThan2(result) {
                        expect(result).not.equal(undefined);
                        expect(result.isValid).to.be.false;
                        expect(result.errors.length).to.equal(1);
                    }
                    function runWithAgeEqual11(result) {
                        var person = new Person('Joe', '', 11);
                        return validio.validate(schemaInt10, person);
                    }
                    function failBecauseAgeIs11andGreatThan10(result) {
                        expect(result).not.equal(undefined);
                        expect(result.isValid).to.be.false;
                        expect(result.errors.length).to.equal(1);
                    }
                });
            });

            describe('Float type on Person.height,', function() {
                var schemaFloat, schemaFloat10;

                it("must create correct schemas", function() {
                    schemaFloat = validio.parse('height float');
                    expect(schemaFloat).to.deep.equal({
                        height: {
                            validators: {
                                isFloat: {}
                            },
                            type: 'float'
                        }
                    });
                    schemaFloat10 = validio.parse('height float 10.5');
                    expect(schemaFloat10).to.deep.equal({
                        height: {
                            validators: {
                                isFloat: {
                                    args: {max: 10.5}
                                }
                            },
                            type: 'float'
                        }
                    });
                });
                it("must validate a simple float", function(done) {
                    var person = new Person('Joe', '', 0, '', '', 10.5);

                    validio.validate(schemaFloat, person)
                    .then(function(result) {
                        expect(result).not.equal(undefined);
                        expect(result.isValid).to.be.true;
                        expect(result.errors.length).to.equal(0);
                    })
                    .then(done)
                    .catch(cacthAndDoneTheError);
                });
                it("must validate a simple float length is less than 10.5", function(done) {
                    var person = new Person('Joe', '', 0, '', '', 10.5);

                    validio.validate(schemaFloat10, person)
                    .then(function(result) {
                        expect(result).not.equal(undefined);
                        expect(result.isValid).to.be.true;
                        expect(result.errors.length).to.equal(0);
                    })
                    .then(done)
                    .catch(cacthAndDoneTheError);
                });
                it("must fail if a simple float length is great than 10.5", function(done) {
                    var person = new Person('Joe', '', 0, '', '', 12.3);

                    validio.validate(schemaFloat10, person)
                    .catch(function(result) {
                        expect(result).not.equal(undefined);
                        expect(result.isValid).to.be.false;
                        expect(result.errors.length).to.equal(1);
                        done();
                    });
                });
                it("must validate a simple float length is between 2.1 and 10.5", function(done) {

                    schemaFloat10.height.validators.isFloat.args = {min: 2.1, max: 10.5};

                    var person = new Person('Joe', '', 0, '', '', 10.5);
                    validio.validate(schemaFloat10, person)
                    .then(succedBecauseAgeIsWithin2_1and10_5)
                    .then(runWithAgeEqual2)
                    .then(dummySucced, failBecauseAgeIs1andLessThan2_1)
                    .then(runWithAgeEqual11_5)
                    .then(dummySucced, failBecauseAgeIs11andGreatThan10_5)
                    .then(done)
                    .catch(cacthAndDoneTheError);

                    function succedBecauseAgeIsWithin2_1and10_5(result) {
                        expect(result).not.equal(undefined);
                        expect(result.isValid).to.be.true;
                        expect(result.errors.length).to.equal(0);
                    }
                    function runWithAgeEqual2(result) {
                        var person = new Person('Joe', '', 0, '', '', 2.0);
                        return validio.validate(schemaFloat10, person);
                    }
                    function failBecauseAgeIs1andLessThan2_1(result) {
                        expect(result).not.equal(undefined);
                        expect(result.isValid).to.be.false;
                        expect(result.errors.length).to.equal(1);
                    }
                    function runWithAgeEqual11_5(result) {
                        var person = new Person('Joe', '', 0, '', '', 11.5);
                        return validio.validate(schemaFloat10, person);
                    }
                    function failBecauseAgeIs11andGreatThan10_5(result) {
                        expect(result).not.equal(undefined);
                        expect(result.isValid).to.be.false;
                        expect(result.errors.length).to.equal(1);
                    }
                });
            });

        });

        describe('Formats,', function() {
            describe('Email format,', function() {
                var schemaEmail, schemaEmail10;

                it("must create correct schema", function() {
                    schemaEmail = validio.parse('email f-email');
                    expect(schemaEmail).to.deep.equal({
                        email: {
                            validators: {
                                isEmail: {},
                                isString: {}
                            },
                            format: 'email',
                            type: 'string'
                        }
                    });
                    schemaEmail10 = validio.parse('email 10 f-email');
                    expect(schemaEmail10).to.deep.equal({
                        email: {
                            validators: {
                                isEmail: {},
                                isString: {},
                                isLength: {
                                    args: {length: 10}
                                }
                            },
                            length: 10,
                            format: 'email',
                            type: 'string'
                        }
                    });
                });
                it("must validate an email format", function(done) {
                    var person = new Person('Joe', 'nick@nick.com');

                    validio.validate(schemaEmail, person)
                    .then(function(result) {
                        expect(result).not.equal(undefined);
                        expect(result.isValid).to.be.true;
                        expect(result.errors.length).to.equal(0);
                    })
                    .then(done)
                    .catch(cacthAndDoneTheError);
                });
                it("must fail an invalid email format", function(done) {
                    var person = new Person('Joe', 'nick@nick.');

                    validio.validate(schemaEmail, person)
                    .catch(function(result) {
                        expect(result).not.equal(undefined);
                        expect(result.isValid).to.be.false;
                        expect(result.errors.length).to.equal(1);
                        done();
                    });
                });
                it("must fail an invalid email length great than 10", function(done) {
                    var person = new Person('Joe', 'nick@nick.com');

                    validio.validate(schemaEmail10, person)
                    .catch(function(result) {
                        expect(result).not.equal(undefined);
                        expect(result.isValid).to.be.false;
                        expect(result.errors.length).to.equal(1);
                        done();
                    });
                });
            });
        });

        describe('Many formats in the same command line,', function() {
            var emailAndPhone, emai10lAndPhone, emai10lAndPhone15;
            it("must return 'email and myPhone' when passing 'email f-email,myPhone f-phone'", function() {
                emailAndPhone = validio.parse('email f-email, myPhone f-phone');
                expect(emailAndPhone).to.deep.equal(
                    {
                        email: {
                            validators: {
                                isEmail: {},
                                isString: {}
                            },
                            format: 'email',
                            type: 'string'
                        },
                        myPhone: {
                            validators: {
                                isMobilePhone: {args: ['en-US']},
                                isString: {}
                            },
                            format: 'phone',
                            type: 'string'
                        }
                    }
                );
            });
            it("must return 'email 10 and myPhone' when passing 'email 10 f-email,myPhone f-phone'", function() {
                emai10lAndPhone = validio.parse('email 10 f-email, myPhone f-phone');
                expect(emai10lAndPhone).to.deep.equal(
                    {
                        email: {
                            validators: {
                                isEmail: {},
                                isString: {},
                                isLength: {
                                    args: {length: 10}
                                }
                            },
                            length: 10,
                            format: 'email',
                            type: 'string'
                        },
                        myPhone: {
                            validators: {
                                isMobilePhone: {args: ['en-US']},
                                isString: {}
                            },
                            format: 'phone',
                            type: 'string'
                        }
                    }
                );
            });
            it("must return 'email 10 and myPhone 15' when passing 'email 10 f-email,myPhone 15 f-phone'", function() {
                emai10lAndPhone15 = validio.parse('email 10 f-email, myPhone 15 f-phone');
                expect(emai10lAndPhone15).to.deep.equal(
                    {
                        email: {
                            validators: {
                                isEmail: {},
                                isString: {},
                                isLength: {
                                    args: {length: 10}
                                }
                            },
                            length: 10,
                            format: 'email',
                            type: 'string'
                        },
                        myPhone: {
                            validators: {
                                isMobilePhone: {args: ['en-US']},
                                isString: {},
                                isLength: {
                                    args: {length: 15}
                                }
                            },
                            length: 15,
                            format: 'phone',
                            type: 'string'
                        }
                    }
                );
            });
        });
    });
});
