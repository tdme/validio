var validio = require('../../index'),
    testData = require('../data/typesTestData'),
    promisio = require('promisio'),
    required = 'r'
    ;

//name, email f-email, age int, phone f-phone, birth date, height float
//name 30, email f-email, age int, phone f-phone, birth date, height float
describe('Data type validations', ()=> {
    var dataTypeTests = testData.dataTypes

    dataTypeTests.forEach((test)=> {

        describe(test.title, ()=> {
            // Build the schema with always required
            var schema = validio.parse(required + ' ' +test.schema.field+ ' ' +test.schema.arguments),
                result;

            test.values.forEach((value)=> {
                it('must ' +(test.mustPass ? 'pass' : 'fail')+ ' when using "' +value+ '"', ()=> {
                    var obj = {}
                    obj[test.schema.field] = value
                    result = validio.validate(schema, obj, {dictionary: test.dictionary});

                    expect(result).not.equal(undefined);
                    expect(result.isValid).equal(test.mustPass);

                    if (!result.isValid && test.message)
                        expect(result.errors[0]).equal(test.message);
                });
            });
        });
    });
});
