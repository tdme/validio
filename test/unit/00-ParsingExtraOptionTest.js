var validio = require('../../index'),
    testData = require('../data/typesTestData'),
    promisio = require('promisio'),
    _ = require('lodash')
    required = 'r'
    ;

//name, email f-email, age int, phone f-phone, birth date, height float
//name 30, email f-email, age int, phone f-phone, birth date, height float
describe('Extras arguments parsing validations', ()=> {
    var extrasParsingTests = testData.extrasParsing

    extrasParsingTests.forEach((test)=> {

        it(test.title, ()=> {
            // Build the schema with always required
            var schema = validio.parse(required + ' ' +test.schema.field+ ' ' +test.schema.arguments),
                result;

            test.mustCreateExtras && expect(schema[test.schema.field].extras).to.exist;
        });
    });
});

describe('Extras arguments parsing validations with many fields', ()=> {
    var extrasParsingOnManyFieldsTests = testData.extrasParsingOnManyFields

    extrasParsingOnManyFieldsTests.forEach((test)=> {

        it(test.title, ()=> {
            // Build the command line for generating the schema
            var cmd = _.map(test.schema, _.property('arguments')).join(', '),
                schema = validio.parse(required +' '+ cmd);

            test.schema.forEach((field)=> {
                field.mustCreateExtras && expect(schema[field.field].extras).to.exist;
            });

        });
    });
});
