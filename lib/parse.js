var validator = require('validator'),
    formats = require('./formats'),

    // Get supported dataTypes
    dataTypes = require('./dataTypes')
    ;

/**
 * Possible input types:
 *  name 30, email f-email, age int, height float 90.5
 *  books book, books rel book
 */
function parse(fieldList) {
    var fields = fieldList.split(",");

    fields = fields.reduce(function(buffer, current, index) {
        var field = {validators: {}};

        /**
         * Saving information about "extras", this parameter is specified when
         * passing the -e option.
         */
        if (current.indexOf('-e ') > 0) {
            field.extras = current.split('-e')[1].trim();
            current = current.split('-e')[0]
        }

        // Continue parsing as normal input command
        current = current.trim().split(" ");

        var name = current.shift(),
            currentChunk;

        if (name == 'r' || name == 'req') {
            field.required = true;
            name = current.shift();
        }

        if(current.length > 2)
            current = current.slice(current.length-2, current.length)

        currentChunk = detectBracketsOnLastChunk(current, field);

        // Verify the last part is appliying some format: "f-email or f-phone"
        if (current.length > 0 && currentChunk.startsWith("f-")) {
            field.format = currentChunk.replace("f-", "");
            current.pop();

            var inheritType = formats.inheritType(field.format);

            if (inheritType) {
                field.type = inheritType.type
                field.validators[inheritType.validator] = {};

                /**
                 * TODO:
                 *  1. Adding default arguments depending of the format.
                 */
                if(field.format == 'phone' || field.format == 'mobilephone') {
                    field.validators[inheritType.validator] = {args: ['en-US']};
                }
            } else
                delete field.format;
        }

        currentChunk = detectBracketsOnLastChunk(current, field);

        // Verify if the used dataType requires the 'args.max' for max length
        if (current.length > 0 && validator.isFloat(currentChunk)) {
            var currentLength = Number(currentChunk);

            if (['int', 'float'].indexOf(current[0]) >= 0) {
                field.type = current[0];
                field.validators['is' + field.type[0].toUpperCase() + field.type.slice(1)] = {
                    args: {max: currentLength}
                };
            } else {
                field.length = currentLength
                field.validators.isLength = {
                    args: {length: field.length}
                };
            }
            current.pop();
        }

        currentChunk = detectBracketsOnLastChunk(current, field);

        // Asumme default type as string if absent
        if (!field.type)
            currentChunk ? field.type = currentChunk : field.type = 'string';

        /**
         * Add a validator into field.validators object, only if the type is a
         * supported type defined on "lib/dataTypes.js" module, by example:
         *  - string, date, boolean, decimal, float, numeric, int
         */
        if(dataTypes[field.type]) {
            !field.validators[dataTypes[field.type].validator] ? field.validators[dataTypes[field.type].validator] = {} : false;
        }
        else {
            delete field.validators;
            field.relatedTo = field.type
            delete field.type;
        }

        buffer[name] = field;
        return buffer;
    }, {});

    return fields;
}

function detectBracketsOnLastChunk(current, field) {
    var currentChunk = current[current.length - 1];

    if (current.length > 0 && currentChunk.startsWith("[") && currentChunk.endsWith("]")) {
        current[current.length - 1] = currentChunk = currentChunk.replace('[', '').replace(']', '');
        field.isArray = true
    }

    return currentChunk;
}

module.exports = parse;
