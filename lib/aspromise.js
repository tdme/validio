var formats = require('./formats'),
    parse = require('./parse'),
    validate = require('./validate')
    ;

function validatePromised(schema, obj) {
    return new Promise(function(onSucced, onRejected) {
        var result = validate(schema, obj);
        result.isValid ? onSucced(result) : onRejected(result);
    });
}

function defaultCatch(result) {
    done(new Error(result.errors));
}
module.exports = {
    parse: parse,
    formats: formats,
    validate: validatePromised,
    defaultCatch: defaultCatch
}
