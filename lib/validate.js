var validator = require('validator'),
    formats = require('./formats'),

    // Get supported dataTypes
    dataTypes = require('./dataTypes'),
    _ = require('lodash')
    ;


function validate(schema, obj, options) {
    var enforcedObj = {}

    var errors = Object.keys(schema).reduce(function(buffer, fieldName, ind) {
        var field = schema[fieldName];

        // Get current value
        var oValue = obj[fieldName] != undefined ? obj[fieldName] : undefined,
            converted = oValue

        if (Array.isArray(oValue)) {
            if (field.isArray) {

                if (_.includes(_.keys(dataTypes), field.type)) {
                    // Looping an array of supported basic types declared on 'lib/dataTypes.js'
                    oValue.forEach((item, index)=>{
                        var valueObj = {};
                        valueObj[fieldName] = item
                        var result = validateOne(schema, valueObj, options, /*buffer,*/ fieldName, ind);

                        result.isValid && (converted[index] = result.enforcedObj);

                        buffer.push.apply(buffer, result.errors);
                    });
                }
                else {
                    // Looping an array of related objects
                    oValue.forEach((item, index)=>{
                        // Calling the recursivity on Validio.validate function
                        var result = validate(options.dictionary[field.relatedTo], item, options);

                        result.isValid && (converted[index] = result.enforcedObj);

                        buffer.push.apply(buffer, result.errors);
                    });
                }
            } else {
                buffer.push(_.template('${field} field value is an array, but it is not defined as array')({field: fieldName}));
            }
        }
        else {
            if (field.isArray) {
                buffer.push(_.template('${field} field is a sinlge value, but it is defined as array on schema')({field: fieldName}));
            } else {
                // Calling the recursivity on Validio.validate function
                var result = validateOne(schema, obj, options, /*buffer,*/ fieldName, ind);
                result.isValid && (converted = result.enforcedObj);
                buffer.push.apply(buffer, result.errors);
            }
        }

        // Update value on new enforced object
        converted != undefined && (enforcedObj[fieldName] = converted)

        return buffer;
    }, []);

    return {errors: errors, isValid: errors.length == 0, enforcedObj: enforcedObj};
}

function validateOne(schema, obj, options, fieldName, ind) {
    var enforcedObj = {}, buffer = [],
        isValid = true,
        field = schema[fieldName];

    // Get current value
    var oValue = obj[fieldName] != undefined ? obj[fieldName] : undefined,
        converted = oValue;

    if (field.relatedTo) {
        if(oValue) {
            if (options && options.dictionary && options.dictionary[field.relatedTo]) {
                // Calling the recursivity on Validio.validate function
                var result = validate(options.dictionary[field.relatedTo], oValue, options);
                result.isValid && (converted = result.enforcedObj);
                buffer.push.apply(buffer, result.errors);
            }
            else {
                buffer.push(_.template('${field} field is related to a "${type}" type not defined on dictionary')({
                    field: fieldName,
                    type: field.relatedTo
                }));
            }
        }
        else {
            if (field.required) {
                var message = _.template('related ${field} field is missing')({ field: fieldName });

                if (!_.includes(buffer, message))
                    buffer.push(message);
            }
        }
    }
    else {
        oValue = oValue == undefined ? oValue : oValue.toString();

        /**
         * Loop over validators and run each one, and error message will be stored
         * if the validation fails
         */
        for (validationFnName of Object.keys(field.validators || {})) {
            var validationFn = formats.getValidator(validationFnName),
                args = field.validators[validationFnName].args

            if(oValue) {
                // Pass parameters and run validator
                var result = validationFn(oValue, args);

                // Add new error if validation fails
                if (!result) {
                    var format = formats.formats[validationFnName.replace('is', '').toUpperCase()];

                    if(format && format.message)
                        buffer.push( _.template(format.message(args))({name: fieldName, args: field.validators[validationFnName].args}) );
                    else {
                        buffer.push(_.template('${field} field is not compliant with ${validator} format')({
                            field: fieldName,
                            validator: validationFnName.replace('is', '').toUpperCase()
                        }));
                    }

                    // Mark as this current field has failed running this validator function
                    isValid = false
                }
            }
            else {
                if (field.required) {
                    var message = _.template('${field} field is missing')({ field: fieldName });
                    !_.includes(buffer, message) && buffer.push(message);
                }
            }
        }

        if(isValid && options && options.enforceTypes) {
            // Get the enforcer function used to convert the value
            var enforcerFn = getEnforcer(field.type);

            if (oValue && enforcerFn) {
                // Force to convert input value to dataTypes following schema definition
                converted = enforcerFn(oValue);//, args);

                if (converted == null || converted == undefined || isNaN(converted)) {
                    buffer.push(_.template('${field} "' +oValue+ '" can not be converted to ${type} type')({
                        field: fieldName,
                        type: field.type
                    }));
                }
            }
        }
    }

    // Update value on new enforced object
    enforcedObj = converted

    return {errors: buffer, isValid: buffer.length == 0, enforcedObj: enforcedObj};
}

function getEnforcer(dataType) {
    // TODO: Throw an Error when "Validator Module" is not supporting a dataType
    return dataTypes[dataType] && dataTypes[dataType].converter ? validator[dataTypes[dataType].converter] : undefined;
}

module.exports = validate;
