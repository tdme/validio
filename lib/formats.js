var validator = require('validator');

function inheritType(format) {
    // //console.log(format);
    return Formats[format.toUpperCase()] ? Formats[format.toUpperCase()] : undefined;
}

var Formats = {
    ALPHA: {
        type: 'string',
        validator: 'isAlpha',
        description: "(str [, locale]) - check if the string contains only letters (a-zA-Z). Locale is one of ['ar', 'ar-AE', 'ar-BH', 'ar-DZ', 'ar-EG', 'ar-IQ', 'ar-JO', 'ar-KW', 'ar-LB', 'ar-LY', 'ar-MA', 'ar-QA', 'ar-QM', 'ar-SA', 'ar-SD', 'ar-SY', 'ar-TN', 'ar-YE', 'cs-CZ', 'de-DE', 'en-AU', 'en-GB', 'en-HK', 'en-IN', 'en-NZ', 'en-US', 'en-ZA', 'en-ZM', 'es-ES', 'fr-FR', 'hu-HU', 'nl-NL', 'pl-PL', 'pt-PT', 'ru-RU', 'tr-TR']) and defaults to en-US."
    },
    ALPHANUMERIC: {
        type: 'string',
        validator: 'isAlphanumeric',
        description: "(str [, locale]) - check if the string contains only letters and numbers. Locale is one of ['ar', 'ar-AE', 'ar-BH', 'ar-DZ', 'ar-EG', 'ar-IQ', 'ar-JO', 'ar-KW', 'ar-LB', 'ar-LY', 'ar-MA', 'ar-QA', 'ar-QM', 'ar-SA', 'ar-SD', 'ar-SY', 'ar-TN', 'ar-YE', 'cs-CZ', 'de-DE', 'en-AU', 'en-GB', 'en-HK', 'en-IN', 'en-NZ', 'en-US', 'en-ZA', 'en-ZM', 'es-ES', 'fr-FR', 'hu-HU', 'nl-NL', 'pl-PL', 'pt-PT', 'ru-RU', 'tr-TR']) and defaults to en-US."
    },
    BOOLEAN: {
        type: 'boolean',
        validator: 'isBoolean',
        description: "(str) - check if a string is a boolean.",
        Xmessage: function (args) {
            // //console.log('args:', args);
            return 'the ${name} must be a valid boolean value';
        }
    },
    CREDITCARD: {
        type: 'string',
        validator: 'isCreditCard',
        description: "(str) - check if the string is a credit card."
    },
    CURRENCY: {
        type: 'string',
        validator: 'isCurrency',
        description: "(str, options) - check if the string is a valid currency amount. options is an object which defaults to {symbol: '$', require_symbol: false, allow_space_after_symbol: false, symbol_after_digits: false, allow_negatives: true, parens_for_negatives: false, negative_sign_before_digits: false, negative_sign_after_digits: false, allow_negative_sign_placeholder: false, thousands_separator: ',', decimal_separator: '.', allow_space_after_digits: false }."
    },
    DATAURI: {
        type: 'string',
        validator: 'isDataURI',
        description: "(str) - check if the string is a data uri format."
    },
    DATE: {
        type: 'date',
        validator: 'isDate',
        description: "(str) - check if the string is a date."
    },
    DECIMAL: {
        type: 'decimal',
        validator: 'isDecimal',
        description: "(str) - check if the string represents a decimal number, such as 0.1, .3, 1.1, 1.00003, 4.0, etc."
    },
    EMAIL: {
        type: 'string',
        validator: 'isEmail',
        description: "(str [, options]) - check if the string is an email. options is an object which defaults to { allow_display_name: false, allow_utf8_local_part: true, require_tld: true }. If allow_display_name is set to true, the validator will also match Display Name <email-address>. If allow_utf8_local_part is set to false, the validator will not allow any non-English UTF8 character in email address' local part. If require_tld is set to false, e-mail addresses without having TLD in their domain will also be matched."
    },
    FQDN: {
        type: 'string',
        validator: 'isFQDN',
        description: "(str [, options]) - check if the string is a fully qualified domain name (e.g. domain.com). options is an object which defaults to { require_tld: true, allow_underscores: false, allow_trailing_dot: false }."
    },
    FLOAT: {
        type: 'float',
        validator: 'isFloat',
        description: "(str [, options]) - check if the string is a float. options is an object which can contain the keys min and/or max to validate the float is within boundaries (e.g. { min: 7.22, max: 9.55 }).",
        message: function (args) {
            // //console.log('args:', args);
            return args ?
            args.min && args.max  ? 'the ${name} must be within ${args.min} and ${args.max}' :
            args.min              ? 'the ${name} can not be less than ${args.min}' :
            args.max              ? 'the ${name} can not be great than ${args.max}' :
                                    'the ${name} must be a valid float value' :
                                    'the ${name} must be a valid float value.';
        }
    },
    HEXADECIMAL: {
        type: 'string',
        validator: 'isHexadecimal',
        description: "(str) - check if the string is a hexadecimal number."
    },
    IP: {
        type: 'string',
        validator: 'isIP',
        description: "(str [, version]) - check if the string is an IP (version 4 or 6)."
    },
    ISBN: {
        type: 'string',
        validator: 'isISBN',
        description: "(str [, version]) - check if the string is an ISBN (version 10 or 13)."
    },
    ISIN: {
        type: 'string',
        validator: 'isISIN',
        description: "(str) - check if the string is an ISIN (stock/security identifier)."
    },
    ISO8601: {
        type: 'string',
        validator: 'isISO8601',
        description: "(str) - check if the string is a valid ISO 8601 date."
    },
    // In', description:"(str, values) - check if the string is in a array of allowed values."},
    INT: {
        type: 'int',
        validator: 'isInt',
        description: "(str [, options]) - check if the string is an integer. options is an object which can contain the keys min and/or max to check the integer is within boundaries (e.g. { min: 10, max: 99 }). options can also contain the key allow_leading_zeroes, which when set to true will accept integer values with leading zeroes (e.g. { allow_leading_zeroes: true }).",
        message: function (args) {
            // console.log('args:', args);
            var extraMsg = 'must be only numbers and'
            return args ?
            args.min && args.max  ? 'the ${name} ' +extraMsg+ ' must be within ${args.min} and ${args.max}' :
            args.min              ? 'the ${name} ' +extraMsg+ ' can not be less than ${args.min}' :
            args.max              ? 'the ${name} ' +extraMsg+ ' can not be great than ${args.max}' :
                                    'the ${name} ' +extraMsg+ ' must be a valid integer value' :
                                    'the ${name} ' +extraMsg+ ' must be a valid integer value.';
        }
    },
    // JSON', description:"(str) - check if the string is valid JSON (note: uses JSON.parse)."},
    MACADDRESS: {
        type: 'string',
        validator: 'isMACAddress',
        description: "(str) - check if the string is a MAC address."
    },
    PHONE: {
        type: 'string',
        validator: 'isMobilePhone',
        description: "(str, locale) - check if the string is a mobile phone number, (locale is one of ['ar-DZ', 'ar-SY', 'cs-CZ', 'de-DE', 'da-DK', 'el-GR', 'en-AU', 'en-GB', 'en-HK', 'en-IN', 'en-NZ', 'en-US', 'en-CA', 'en-ZA', 'en-ZM', 'es-ES', 'fi-FI', 'fr-FR', 'hu-HU', 'ms-MY', 'nb-NO', 'nn-NO', 'pl-PL', 'pt-PT', 'ru-RU', 'tr-TR', 'vi-VN', 'zh-CN', 'zh-TW'])."
    },
    MOBILEPHONE: {
        type: 'string',
        validator: 'isMobilePhone',
        description: "(str, locale) - check if the string is a mobile phone number, (locale is one of ['ar-DZ', 'ar-SY', 'cs-CZ', 'de-DE', 'da-DK', 'el-GR', 'en-AU', 'en-GB', 'en-HK', 'en-IN', 'en-NZ', 'en-US', 'en-CA', 'en-ZA', 'en-ZM', 'es-ES', 'fi-FI', 'fr-FR', 'hu-HU', 'ms-MY', 'nb-NO', 'nn-NO', 'pl-PL', 'pt-PT', 'ru-RU', 'tr-TR', 'vi-VN', 'zh-CN', 'zh-TW'])."
    },
    MONGOID: {
        type: 'string',
        validator: 'isMongoId',
        description: "(str) - check if the string is a valid hex-encoded representation of a MongoDB ObjectId."
    },
    NUMERIC: {
        type: 'int',
        validator: 'isNumeric',
        description: "(str) - check if the string contains only numbers."
    },
    URL: {
        type: 'string',
        validator: 'isURL',
        description: "(str [, options]) - check if the string is an URL. options is an object which defaults to { protocols: ['http','https','ftp'], require_tld: true, require_protocol: false, require_valid_protocol: true, allow_underscores: false, host_whitelist: false, host_blacklist: false, allow_trailing_dot: false, allow_protocol_relative_urls: false }."
    },
    UUID: {
        type: 'string',
        validator: 'isUUID',
        description: "(str [, version]) - check if the string is a UUID (version 3, 4 or 5)."
    },
    UPPERCASE: {
        type: 'string',
        validator: 'isUppercase',
        description: "(str) - check if the string is uppercase."
    },

    // Custom validators
    STRING: {
        type: 'string',
        validator: 'isString',
        description: '(str) -  check if str is a string, it can accept any character from the asscii table',
        custom: true
    },
    LENGTH: {
        type: 'string',
        validator: 'isLength',
        description: '(str) -  check if str is a string less than or equal that given length',
        custom: true,
        message: function (args) {
            // //console.log('args:', args);
            return args ?
                   args.length ? 'the ${name} length can not be great than ${args.length}' :
                                 'the ${name} must be a valid string value' :
                                 'the ${name} must be a valid string value';
        }
    }
};

function isString(str) {
    return typeof str === 'string';
}

function isLength(str, args) {
    return str.length <= args.length;
}

/**
 * Validator is returning tru when passing numeric numbers, so we are validating
 * that no numerics are acepted
 */
function isDate(str, args) {
    return validator.isDate(str, args) && !validator.isNumeric(str);
}


function getValidator(functionName) {
    // console.log('functionName:', functionName);
    return functionName in module.exports ? module.exports[functionName] : validator[functionName];
}

module.exports = {
    formats : Formats,
    inheritType : inheritType,
    getValidator : getValidator,
    isString: isString,
    isLength: isLength,
    isDate: isDate
}
