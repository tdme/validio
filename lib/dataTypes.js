/**
 * Define the supported data types on the module, here we are going to add supported
 * types as needed. If a type is not defined here, then it will try to validate as a
 * custom type and must have and schema previously defined in the dictionary.
 */
var supportedDataTypes = {
    string: {
        validator: 'isString'
    },
    date: {
        validator: 'isDate',
        converter: 'toDate'
    },
    boolean: {
        validator: 'isBoolean',
        converter: 'toBoolean'
    },
    decimal: {
        validator: 'isDecimal'
    },
    float: {
        validator: 'isFloat',
        converter: 'toFloat'
    },
    numeric: {
        validator: 'isNumeric'
    },
    int: {
        validator: 'isInt',
        converter: 'toInt'
    }
}

module.exports = supportedDataTypes;
